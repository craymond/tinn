# Tinn++

C++ portage of [Tinn](https://github.com/glouw/tinn) (Tiny Neural Network)

## Features

* Portable - Runs where a C++14 compiler is present.

* Sigmoidal activation.

* One hidden layer.

## Tips

* Tinn++ will never use more than the C++ standard library.

* Tinn++ is great for embedded systems. Train a model on your powerful desktop and load
it onto a microcontroller and use the analog to digital converter to predict real time events.

## install

- git clone https://gitlab.inria.fr/craymond/tinn.git
- cmake . (in the tinn directory)
- make

## sample data + evaluation

* [data/optidigits.(tra/tes)](https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits)
* evaluation thanks to [metrics](https://gitlab.inria.fr/craymond/metrics)
