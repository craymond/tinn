#pragma once
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <cassert>
#include "Data.h"

using vector=std::vector<reel>;
constexpr const reel UN=static_cast<reel>(1);


class Tinn
{
    // Input weights.
    vector w;
    // Hidden to output layer weights.
    vector x;
    // Biases.
    vector b;
    // Hidden layer.
    vector h;
    // Output layer.
    vector o;
    // Number of biases - always two - Tinn only supports a single hidden layer.
    constexpr static uint nb=2;
    // Number of inputs.
    uint _nips;
    // Number of hidden neurons.
    uint _nhid;
    // Number of outputs.
    uint _nops;

    // Computes error.
    constexpr static reel err(const reel a, const reel b) { return 0.5 * (a - b) * (a - b); }
    // Returns partial derivative of error function.
    constexpr static reel pderr(const reel a, const reel b)  { return a - b; }
    // Computes total error of target to output.
    reel toterr(cuint label, const vector& o) const
    {
        vector tg(o.size(),0);
        tg[label]=1;

        reel sum = 0.0;
        for(uint i = 0; i < tg.size(); i++)
            sum += err(tg[i], o[i]);
        return sum;
    }
    // Activation function.
    static reel act(const reel a) { return UN / (UN + exp(-a)); }

    // Returns partial derivative of activation function.
    constexpr static reel pdact(const reel a) { return a * (UN - a); }

    // Randomizes tinn weights and biases.
    void initWeights();
    void build();
    void fprop(const vector&  in);
    void bprop(const vector& in, cuint tg, const reel rate);
    reel train( const vector& in, cuint tg, const reel rate);
public:
    Tinn() = default;
    Tinn(cuint nips, cuint nhid, cuint nops);

    uint predict(const vector& in);
    std::vector<uint> predict(const Data& d);

    void fit(Data& in, cuint nbIterations=10,reel rate=1, const reel anneal=0.99);

    void save(const string& path) const;

    void load(const string& path);

};