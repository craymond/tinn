#include "Data.h"

const std::vector<uint>& Data::targets() const
{
    return tg;
}

std::vector<reel> Data::normalize(const std::vector<reel>& m)
{
    std::vector<reel> max(this->inputSize(),0);
    if(!max.empty())
        max=m;
    else
    {
        for(uint i=0;i<max.size();++i)
            for(uint j=0;j<this->rows();++j)
                max[i]=std::max(max[i],in[j][i]);
    }
    for(uint i=0;i<this->rows();++i)
        for(uint j=0;j<max.size();++j)
            in[i][j]/=max[i];
    return max;
}

void Data::check() const
{
    const auto nbf=this->inputSize();
    auto prob=std::find_if(in.begin(),in.end(),[nbf](const auto& v){return v.size()!=nbf;});
    if(prob!=in.end())
        throw std::runtime_error("sample "+std::to_string(prob-in.begin())+" contains only "+std::to_string(prob->size())+" features");
}

void Data::parse(const string& line, cuint row)
{
    reel tmp;
    std::istringstream fin(line);
    while(fin>>tmp)
    {
        in[row].push_back(tmp);       
    }
    tg[row]=in[row].back();//the last read element was the target, so we move it
    //nops=std::max(nops,static_cast<uint>(in[row].back())+1); //to memorise the number of target
    in[row].pop_back();
}

void Data::shuffle()
{
    std::random_device rd;
    std::uniform_int_distribution<uint> dist(0,this->inputSize());
    std::default_random_engine gen(rd());
    for(uint a = 0; a < in.size(); ++a)
    {
        cuint b = dist(gen);
        std::swap(tg[a],tg[b]);
        // Swap input.
        in[a].swap(in[b]);
        
    }
    check();
}

Data::Data(const string& path)
{
    std::ifstream file(path);
    if(!file)
    {
        std::cerr<<"Could not open "<< path<<"\n";
        std::cerr<<"Get it from the machine learning database: ";
        std::cerr<<"wget https://archive.ics.uci.edu/ml/machine-learning-databases/optdigits/optdigits.tra\n";
        std::cerr<<"wget https://archive.ics.uci.edu/ml/machine-learning-databases/optdigits/optdigits.tes\n";
        exit(1);
    }
    std::string line;
    for(uint i=0; std::getline(file,line);++i)
    {
        
        in.emplace_back();
        tg.emplace_back();
        parse(line, i);
    }
    nops=*std::max_element(tg.cbegin(),tg.cend())+1;
    check();

}