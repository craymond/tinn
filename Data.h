#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <random>
#include <iostream>
#include <algorithm>
using std::string;
using reel=double;
using uint =unsigned int;
using cuint = const uint;

// Data object.
class Data
{
    // 2D floating point array of input.
    std::vector<std::vector<reel>> in;
    // number associated to the target
    std::vector<uint> tg;
    // Number of outputs to neural network.
    uint nops=0;

    //check if read data are consistant (same number of features for each sample)
    void check() const;
    // Gets one row of inputs and outputs from a string.
    void parse(const string& line, cuint row);
public:
    // constructor from file
    explicit Data(const string& path);
    // normalize features, for now, just by the max and considering a min=0
    std::vector<reel> normalize(const std::vector<reel>& max={});
    // get number of samples
    uint rows() const {return in.size();}
    // get number of features (or input neurons)
    uint inputSize() const {return in[0].size();}
    // get number of target classes (or output neurons)
    uint outputSize() const {return nops;}
    const std::vector<reel>& x(cuint i) const {return in[i];}
    uint y(cuint i) const {return tg[i];} 
    const std::vector<uint>& targets() const; 
    // Randomly shuffles a data object.
    void shuffle();

};