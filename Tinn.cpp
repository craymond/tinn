#include "Tinn.hpp"


// Performs back propagation.
void Tinn::bprop( const vector& in, cuint label, const reel rate)
{
    vector tg(_nops,0);
    tg[label]=1;

    for(uint i = 0; i < _nhid; i++)
    {
        reel sum = 0.0;
        // Calculate total error change with respect to output.
        for(uint j = 0; j < _nops; j++)
        {
            const reel a = pderr(o[j], tg[j]);
            const reel b = pdact(o[j]);
            sum += a * b * x[j * _nhid + i];
            // Correct weights in hidden to output layer.
            x[j * _nhid + i] -= rate * a * b * h[i];
        }
        // Correct weights in input to hidden layer.
        for(uint j = 0; j < _nips; j++)
            w[i * _nips + j] -= rate * sum * pdact(h[i]) * in[j];
    }
}

// Performs forward propagation.
void Tinn::fprop(const vector&  in)
{
    // Calculate hidden layer neuron values.
    for(uint i = 0; i < _nhid; i++)
    {
        reel sum = 0.0;
        for(uint j = 0; j < _nips; j++)
        {
            sum += in[j] * w[i * _nips + j];
        }
        h[i] = act(sum + b[0]);
    }

    // Calculate output layer neuron values.
    for(uint i = 0; i < _nops; i++)
    {
        reel sum = 0.0;
        for(uint j = 0; j < _nhid; j++)
            sum += h[j] * x[i * _nhid + j];
        o[i] = act(sum + b[1]);
    }
}



// Returns an output prediction given an input.
uint Tinn::predict(const vector& in)
{
    fprop(in);
    return std::max_element(o.begin(),o.end())-o.begin();
}

std::vector<uint> Tinn::predict(const Data& d)
{
    std::vector<uint> res(d.rows());
    for(uint i=0;i<d.rows();++i)
        res[i]=predict(d.x(i));
    return res;
}

// Trains a tinn with an input and target output with a learning rate. Returns target to output error.
reel Tinn::train(const vector& in, cuint tg, const reel rate)
{
    fprop(in);
    bprop(in, tg, rate);
    return toterr(tg, o);
}

void Tinn::fit(Data& data,cuint iterations,reel rate,const reel anneal)
{
    for(uint i = 0; i < iterations; i++)
    {
        data.shuffle();
        reel error = 0;
        for(uint j = 0; j < data.rows(); j++)
        {
            error += this->train(data.x(j), data.y(j), rate);
        }
        std::cout<<"\repoch "<<i<<" loss="<<(error / data.rows()) << std::flush;// :: learning rate "<<rate<<"\n";
        rate *= anneal;
    }
    std::cout<<"\n\n";
}

void Tinn::initWeights()
{
    std::random_device rd;
    std::uniform_real_distribution<reel> dist(-0.5,0.5);
    std::default_random_engine eng(rd());
    for(auto& e: w) e = dist(eng);
    for(auto& e: b) e = dist(eng);
}

void Tinn::build()
{
    // Tinn only supports one hidden layer so there are two biases.
    w.resize(_nhid*_nips);
    x.resize(_nhid*_nops); 
    b.resize(nb); 
    h.resize(_nhid);
    o.resize(_nops);
    initWeights();
    
}

// Constructs a tinn with number of inputs, number of hidden neurons, and number of outputs
Tinn::Tinn(cuint nips, cuint nhid, cuint nops):  _nips(nips),_nhid(nhid),_nops(nops)
{  
   build();
}

// Saves a tinn to disk.
void Tinn::save(const string& path) const
{
    std::ofstream file(path);
    // Save header.
    file << _nips <<" "<< _nhid << " " << _nops<<'\n';
    // Save biases and weights.
    std::copy(b.begin(),b.end(),std::ostream_iterator<reel>(file," "));
    file << "\n";
    std::copy(w.begin(),w.end(),std::ostream_iterator<reel>(file," "));
    file << "\n";
    std::copy(x.begin(),x.end(),std::ostream_iterator<reel>(file," "));
    //for(int i = 0; i < t.nb; i++) fprintf(file, "%f\n", (double) t.b[i]);
    //for(int i = 0; i < t.nw; i++) fprintf(file, "%f\n", (double) t.w[i]);

}

// Loads a tinn from disk.
void Tinn::load(const string& path)
{
    std::ifstream file(path);
    file >> _nips >> _nhid >> _nops;
    // Build a new tinn.
    build();
    // Load bias and weights.
    for(auto& e: b) file >> e; 
    for(auto& e: w) file >> e; 
    for(auto& e: x) file >> e; 
}

