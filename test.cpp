#include <iostream>
#include <sstream>
#include <string>
#include "Tinn.hpp"
#include "metrics/Metrics.hpp"


// Learns and predicts hand written digits with 98% accuracy.
int main(int argc,char* argv[])
{
    if(argc!=3) 
    {
        std::cerr << "Use "<<argv[0] << " <hiddenNeuron> <epochs>\n";
        return -1;
    }
    cuint hiddenNeurons=std::atoi(argv[1]);
    cuint epochs=std::atoi(argv[2]);
    // Load the training set.
    Data data("data/optdigits.tra");
    auto param=data.normalize();
    std::cout << "data with "<<data.rows()<<" samples code with "<<data.inputSize()<<" features and "<<data.outputSize()<<" target classes\n";
    // Train, baby, train.
    Tinn tinn(data.inputSize(), hiddenNeurons, data.outputSize());
    
    tinn.fit(data,epochs);
    // This is how you save the neural network to disk.
    tinn.save("saved.tinn");

    // This is how you load the neural network from disk.
    Tinn loaded;
    loaded.load("saved.tinn");
    // Now we do a prediction with the neural network we loaded from disk.
    // Ideally, we would also load a testing set to make the prediction with,
    // but for the sake of brevity here we just reuse the training set from earlier.
    { 
    const auto pd = loaded.predict(data);
    // Prints target.
    metrics::classificationReport report(data.outputSize());
    report(data.targets(),pd);
    std::cout <<"Eval on training data\n"<< report.getAsText();
    }

    Data test("data/optdigits.tes");
    test.normalize(param);
    const auto pd = loaded.predict(test);
    // Prints target.
    metrics::classificationReport report(test.outputSize());
    report(test.targets(),pd);
    std::cout << "\nEval on test  data\n"<< report.getAsText();
   
    return 0;
}
